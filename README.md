# Geofeed
This repo is here to host the community fibre geofeed file. The geofeed file specifies a geographical location for all our currently advertised IPv4 / IPv6 blocks.   

## Getting started
To download the file you can directly use [this link](https://gitlab.com/cflpublic/geofeed/-/raw/main/AS201838_geofeed.csv?inline=false)

Alternatively the file can be viewed [raw](https://gitlab.com/cflpublic/geofeed/-/raw/main/AS201838_geofeed.csv)
